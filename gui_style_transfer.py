# -*- coding: utf-8 -*-
"""
 Descripcion: programa para cambiar de estilo a una imagen
"""
import sys
from PyQt5 import QtWidgets

from UI.classwindow import MainWindow
if __name__ == "__main__":
    APP = QtWidgets.QApplication(sys.argv)
    MAIN_WINDOW = MainWindow()
    MAIN_WINDOW.show()
    sys.exit(APP.exec_())
    