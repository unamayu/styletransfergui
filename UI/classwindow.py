# -*- coding: utf-8 -*-

import functools
import os
import datetime
from matplotlib import gridspec
import matplotlib.pylab as plt
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap

from shutil import copyfile

from UI.main import Ui_MainWindow

def crop_center(image):
  """Returns a cropped square image."""
  shape = image.shape
  new_shape = min(shape[1], shape[2])
  offset_y = max(shape[1] - shape[2], 0) // 2
  offset_x = max(shape[2] - shape[1], 0) // 2
  image = tf.image.crop_to_bounding_box(
      image, offset_y, offset_x, new_shape, new_shape)
  return image

@functools.lru_cache(maxsize=None)
def load_image(image_url, image_size=(256, 256), preserve_aspect_ratio=True):
  """Loads and preprocesses images."""
  # Cache image file locally.
  image_path = tf.keras.utils.get_file(os.path.basename(image_url)[-128:], image_url)
  # Load and convert to float32 numpy array, add batch dimension, and normalize to range [0, 1].
  img = plt.imread(image_path).astype(np.float32)[np.newaxis, ...]
  if img.max() > 1.0:
    img = img / 255.
  if len(img.shape) == 3:
    img = tf.stack([img, img, img], axis=-1)
  img = crop_center(img)
  img = tf.image.resize(img, image_size, preserve_aspect_ratio=True)
  return img

@functools.lru_cache(maxsize=None)
def load_image_local(image_path, image_size=(256, 256), preserve_aspect_ratio=True):
  """Loads and preprocesses images."""
  # Cache image file locally.
 # image_path = tf.keras.utils.get_file(os.path.basename(image_url)[-128:], image_url)
  # Load and convert to float32 numpy array, add batch dimension, and normalize to range [0, 1].
  img = plt.imread(image_path).astype(np.float32)[np.newaxis, ...]
  if img.max() > 1.0:
    img = img / 255.
  if len(img.shape) == 3:
    img = tf.stack([img, img, img], axis=-1)
  img = crop_center(img)
  img = tf.image.resize(img, image_size, preserve_aspect_ratio=True)
  return img

def show_n(images, titles=('',)):
  n = len(images)
  image_sizes = [image.shape[1] for image in images]
  w = (image_sizes[0] * 6) // 320
  plt.figure(figsize=(w  * n, w))
  gs = gridspec.GridSpec(1, n, width_ratios=image_sizes)
  for i in range(n):
    plt.subplot(gs[i])
    plt.imshow(images[i][0], aspect='equal')
    plt.axis('off')
    plt.title(titles[i] if len(titles) > i else '')
  plt.savefig("new.png")
  plt.show()

class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    """Ventana principal."""
    def __init__(self, *args, **kwargs):
        QtWidgets.QMainWindow.__init__(self, *args, **kwargs)
        self.setupUi(self)
        self.btSave.hide()
        self.setWindowTitle("Photo Style Transefer");
        self.btOriginal.clicked.connect(self.load_original)
        self.btStyle.clicked.connect(self.load_style)
        self.btRun.clicked.connect(self.run)
        self.btSave.clicked.connect(self.save)
        self.n_imagenes = 0

    def load_original(self):
        filePath, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', 'E:\\PROGRAMACION\\CURSOS\\OTROS\\TENSORFLOW\\IMAGENES\\')
        if filePath != "":
            print(filePath)
            content_img_size = (384,384)
            self.content_image = load_image_local(filePath, content_img_size)
            self.lbOriginal.setPixmap(QPixmap(filePath).scaledToWidth(256))
            self.n_imagenes = self.n_imagenes +1
                         
    def load_style(self):
        filePath, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', 'E:\\PROGRAMACION\\CURSOS\\OTROS\\TENSORFLOW\\IMAGENES\\')
        if filePath != "":
            style_img_size = (256, 256)
            self.style_image = load_image_local(filePath, style_img_size)
            self.lbStyle.setPixmap(QPixmap(filePath).scaledToWidth(256))

    def save(self):
        savePath, _ = QtWidgets.QFileDialog.getSaveFileName(self, 'Open file', 'E:\\PROGRAMACION\\CURSOS\\OTROS\\TENSORFLOW\\IMAGENES\\')
        copyfile('new.png', savePath)

    def run(self):
        print(datetime.datetime.now())
        try:
            print('iniciando descarga:', datetime.datetime.now())
           
            hub_handle = 'https://tfhub.dev/google/magenta/arbitrary-image-stylization-v1-256/2'
            print('iniciando carga:', datetime.datetime.now())
            hub_module = hub.load(hub_handle)
            print(datetime.datetime.now())
            self.outputs = hub_module(tf.constant(self.content_image), 
                                      tf.constant(self.style_image)
                                      )
            show_n([self.outputs[0]])
            print(datetime.datetime.now())
            self.lbFinal.setPixmap(QPixmap('new.png').scaledToWidth(256))
            self.btSave.show()
        except:
            pass
            