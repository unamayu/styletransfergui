# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowModality(QtCore.Qt.ApplicationModal)
        MainWindow.resize(1074, 463)
        MainWindow.setStyleSheet("background-color: rgb(56, 56, 56);\n"
"background-color: rgb(29,29, 29);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 10, 341, 31))
        self.label.setStyleSheet("font: 18pt \"Impact\";\n"
"\n"
"color: rgb(105, 121, 248);")
        self.label.setTextFormat(QtCore.Qt.PlainText)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(19, 66, 201, 21))
        self.label_2.setStyleSheet("font: 12pt \"SF UI  Text\";\n"
"color: rgb(255, 255, 255);")
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(19, 85, 281, 16))
        self.label_3.setStyleSheet("font: 6pt \"SF UI  Text\";\n"
"color: rgb(255, 255, 255);")
        self.label_3.setScaledContents(False)
        self.label_3.setIndent(0)
        self.label_3.setObjectName("label_3")
        self.btOriginal = QtWidgets.QPushButton(self.centralwidget)
        self.btOriginal.setGeometry(QtCore.QRect(227, 65, 75, 23))
        self.btOriginal.setStyleSheet("background-color: rgb(105, 121, 248);\n"
"font: 8pt \"SF UI  Text\";")
        self.btOriginal.setObjectName("btOriginal")
        self.lbOriginal = QtWidgets.QLabel(self.centralwidget)
        self.lbOriginal.setGeometry(QtCore.QRect(30, 110, 256, 256))
        self.lbOriginal.setMinimumSize(QtCore.QSize(256, 256))
        self.lbOriginal.setMaximumSize(QtCore.QSize(256, 256))
        self.lbOriginal.setStyleSheet("background-color: rgb(63, 51, 86);")
        self.lbOriginal.setAlignment(QtCore.Qt.AlignCenter)
        self.lbOriginal.setObjectName("lbOriginal")
        self.lbStyle = QtWidgets.QLabel(self.centralwidget)
        self.lbStyle.setGeometry(QtCore.QRect(390, 110, 256, 256))
        self.lbStyle.setMinimumSize(QtCore.QSize(256, 256))
        self.lbStyle.setMaximumSize(QtCore.QSize(256, 256))
        self.lbStyle.setStyleSheet("background-color: rgb(63, 51, 86);")
        self.lbStyle.setAlignment(QtCore.Qt.AlignCenter)
        self.lbStyle.setObjectName("lbStyle")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(369, 66, 201, 21))
        self.label_4.setStyleSheet("font: 12pt \"SF UI  Text\";\n"
"color: rgb(255, 255, 255);")
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(369, 85, 281, 16))
        self.label_5.setStyleSheet("font: 6pt \"SF UI  Text\";\n"
"color: rgb(255, 255, 255);")
        self.label_5.setScaledContents(False)
        self.label_5.setIndent(0)
        self.label_5.setObjectName("label_5")
        self.btStyle = QtWidgets.QPushButton(self.centralwidget)
        self.btStyle.setGeometry(QtCore.QRect(575, 65, 75, 23))
        self.btStyle.setStyleSheet("background-color: rgb(105, 121, 248);\n"
"font: 8pt \"SF UI  Text\";")
        self.btStyle.setObjectName("btStyle")
        self.lbFinal = QtWidgets.QLabel(self.centralwidget)
        self.lbFinal.setGeometry(QtCore.QRect(740, 110, 256, 256))
        self.lbFinal.setMinimumSize(QtCore.QSize(256, 256))
        self.lbFinal.setMaximumSize(QtCore.QSize(256, 256))
        self.lbFinal.setStyleSheet("background-color: rgb(63, 51, 86);")
        self.lbFinal.setAlignment(QtCore.Qt.AlignCenter)
        self.lbFinal.setObjectName("lbFinal")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(719, 66, 201, 21))
        self.label_6.setStyleSheet("font: 12pt \"SF UI  Text\";\n"
"color: rgb(255, 255, 255);")
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(719, 85, 281, 16))
        self.label_7.setStyleSheet("font: 6pt \"SF UI  Text\";\n"
"color: rgb(255, 255, 255);")
        self.label_7.setScaledContents(False)
        self.label_7.setIndent(0)
        self.label_7.setObjectName("label_7")
        self.btRun = QtWidgets.QPushButton(self.centralwidget)
        self.btRun.setGeometry(QtCore.QRect(927, 64, 75, 23))
        self.btRun.setStyleSheet("background-color: rgb(105, 121, 248);\n"
"font: 8pt \"SF UI  Text\";")
        self.btRun.setObjectName("btRun")
        self.btSave = QtWidgets.QPushButton(self.centralwidget)
        self.btSave.setGeometry(QtCore.QRect(830, 380, 75, 23))
        self.btSave.setStyleSheet("background-color: rgb(190, 82, 142);\n"
"font: 8pt \"SF UI  Text\";")
        self.btSave.setObjectName("btSave")
        self.btSave_2 = QtWidgets.QPushButton(self.centralwidget)
        self.btSave_2.setGeometry(QtCore.QRect(990, 10, 75, 23))
        self.btSave_2.setStyleSheet("background-color: rgb(255, 162, 107);\n"
"font: 8pt \"SF UI  Text\";")
        self.btSave_2.setObjectName("btSave_2")
        self.label.raise_()
        self.label_2.raise_()
        self.label_3.raise_()
        self.lbOriginal.raise_()
        self.lbStyle.raise_()
        self.label_4.raise_()
        self.label_5.raise_()
        self.btStyle.raise_()
        self.lbFinal.raise_()
        self.label_6.raise_()
        self.label_7.raise_()
        self.btRun.raise_()
        self.btOriginal.raise_()
        self.btSave.raise_()
        self.btSave_2.raise_()
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionStyle_Transfer = QtWidgets.QAction(MainWindow)
        font = QtGui.QFont()
        font.setFamily("SF UI  Text")
        font.setPointSize(10)
        self.actionStyle_Transfer.setFont(font)
        self.actionStyle_Transfer.setIconVisibleInMenu(True)
        self.actionStyle_Transfer.setObjectName("actionStyle_Transfer")
        self.actionPiedras_Papel_Tijeras = QtWidgets.QAction(MainWindow)
        self.actionPiedras_Papel_Tijeras.setObjectName("actionPiedras_Papel_Tijeras")

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "|  Style Transfer"))
        self.label_2.setText(_translate("MainWindow", "Original image file"))
        self.label_3.setText(_translate("MainWindow", "────────────────────────────────────────────────────────────────────────────────────────────────────"))
        self.btOriginal.setText(_translate("MainWindow", "Load"))
        self.lbOriginal.setText(_translate("MainWindow", "Original Image"))
        self.lbStyle.setText(_translate("MainWindow", "Style Image"))
        self.label_4.setText(_translate("MainWindow", "Style image file"))
        self.label_5.setText(_translate("MainWindow", "────────────────────────────────────────────────────────────────────────────────────────────────────"))
        self.btStyle.setText(_translate("MainWindow", "Load"))
        self.lbFinal.setText(_translate("MainWindow", "Final Image"))
        self.label_6.setText(_translate("MainWindow", "Stylized"))
        self.label_7.setText(_translate("MainWindow", "────────────────────────────────────────────────────────────────────────────────────────────────────"))
        self.btRun.setText(_translate("MainWindow", "Generate"))
        self.btSave.setText(_translate("MainWindow", "Save"))
        self.btSave_2.setText(_translate("MainWindow", "Credits"))
        self.actionStyle_Transfer.setText(_translate("MainWindow", "Style Transfer"))
        self.actionPiedras_Papel_Tijeras.setText(_translate("MainWindow", "Piedras, Papel, Tijeras"))

